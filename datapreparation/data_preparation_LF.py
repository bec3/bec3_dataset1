# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 11:47:33 2022

@author: adria036

-------------------------------------------------------------------------------
Script for data preparation


- UWB data, 1/s, imputed, filtered
- Milk production data
- Treatments, reproduction
- MPR data


"""

import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\BEC3\scripts\bec3_dataset1\datapreparation") 


#%% import modules

import os
from datetime import date, timedelta
import pandas as pd
import numpy as np
#import copy
#import matplotlib.pyplot as plt
from filter_data import filter_interp_withacc
#from read_data_sewio import read_all_data, read_ids, combine_data_id
from read_data_sewio_LF import read_ids, combine_data_id, read_ids_LF, read_all_data_hour, read_all_data_hour_extra
import sqlite3


#%% set filepaths and dates

# set path to data X,y
path_os = os.path.join(
    "W:","\ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio"
    )

# set path to excel file with cowids
path = path_os

# set path to sql data folder
path_sql = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_total"
    )

# set path to results folder
path_res = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                        "iAdriaens_doc","Projects","cKamphuis","nlas","data","preprocessed"
                        )

# set file name for cowid information
fn = "\Copy_of_Sewio_tags.xlsx"

# set start and end dates
startdate = date(2022,11,27)
enddate = date(2022,11,30)

# set parameters for the imputation and interpolation
win_med = 11
gap_thres = 180


#%% write loop for data loading, editing and saving
t_interval = enddate - startdate
# read zones for area assignment
fna = "\\barn_areas.xlsx"
area_zones = pd.read_excel(path+fna, sheet_name = "areas")    
barn_edges = pd.read_excel(path+fna, sheet_name = "edges")

for dd in range(t_interval.days+1):
    dd_0 = timedelta(dd-1)
    dd = timedelta(dd)
    print(startdate+dd)
    
    # --------------------------read & select data-----------------------------
    #try:
    # -----------------------read the data---------------------------------
    data = read_all_data_hour(startdate+dd,startdate+dd,path_os)
    data_extra = read_all_data_hour_extra(startdate+dd_0, path_os)
    
    data = pd.concat([data_extra,data])
    data = data.sort_values(by = ["alias","feed_reference","at"]).reset_index(drop=1)
    del data_extra        
    # add selection step to correct for time > only day "startdate + dd" retained
    data = data.loc[data["date"].dt.date == startdate+dd,:]
    data = data.reset_index(drop=1)

    # ------------------------if no position data------------------------------
    # read alias from file
    alias = pd.read_excel(path_os+fn, sheet_name = "alias")
    data = pd.merge(data,alias,how = "outer", on = "title")
    data = data.drop(columns = ["alias_x"])
    data = data.rename(columns = {"alias_y":"alias"})
    # delete rows of alias with no data
    data = data.loc[~data["alias"].isna(),:]
    data = data.loc[~data["at"].isna(),:]
    #test = data.loc[data["alias"].isna(),:]
    
    #-------------------------data selection 1/s---------------------------            
    # summarize data (X,y,acc) per unique second (0 to 86399)
    data["day"] = data["date"].dt.day
    data["hour"] = data["at"].dt.hour - data["date"].dt.hour
    data.loc[data["hour"]<0,"hour"] = 24 + data.loc[data["hour"]<0,"hour"]
    data["minute"] = data["at"].dt.minute
    data["second"] = data["at"].dt.second
    data["relsec"] = data["hour"]*3600 \
                     + data["minute"]*60 \
                     + data["second"]
    
    # summarize acc and position data
    new = data[["alias","feed_reference","date","relsec","acc_x","acc_y","acc_z","X","y"]].groupby(by = ["alias","feed_reference","date","relsec"]).mean().reset_index()
    new = new.sort_values(by = ["alias","feed_reference","relsec"])
    if "X" not in new.columns:
        new["X"] = np.nan
    if "y" not in new.columns:
        new["y"] = np.nan
        
    idx = data[["alias","feed_reference","date","relsec"]].drop_duplicates()        
    new["at"] = data.loc[idx.index.values,:]["at"].values
    new["alias"] = pd.to_numeric(new["alias"])
    new = new.drop(columns = ["date"],axis =1)
    new["date"] = pd.to_datetime(new["at"].dt.date, format = "%Y-%m-%d")
    del data, idx
    
    # 'new' contains one record (acc_x, acc_y, acc_z, X, y) per second per tag
    # this frame can be used to add tags to cow ids         
    #----------------------------------------------------------------------
    
    # read id/tag data
    tags = read_ids(path, fn)
    tagsLF = read_ids_LF(path, fn)    
    tags = pd.concat([tags,tagsLF],axis = 0)
    tags = tags.sort_values(by = ["cow","datetime_begin"]).reset_index(drop=1)
    del tagsLF

    # combine it with data
    data,no_ids = combine_data_id(new,tags)
    if len(no_ids)>0:
        no_ids.to_csv(path_res+"/missing_id_records/" + str(startdate).replace("-","") + "_missing_ids.txt", index=False)
    del no_ids,new
    
    # remaining tags without cowID = during the changing
        # remove those values
    data = data.loc[data["cowid"] != 0,:]
    data = data.sort_values(by=["cowid","at"]).reset_index(drop=1)
    data = data[["cowid","alias","feed_reference","barn","date","at","relsec","X","y","acc_x","acc_y","acc_z"]]  
    
    # unfiltered data - one per second or less, all cows included  
    # calculate gaps based on numeric time relative to start of dataset
    data["gap"] = data["relsec"].diff()
    data.loc[data["gap"] < 0,"gap"] = np.nan # set to nan when different cowid
    
    #-----------------------filter & interpolate per cow-----------------------
    # filter the (X,y) time series with a median filter, minimal dist travelled and interpolate
    # cows in the barn
    cows = data["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
    cows = pd.DataFrame(cows)
    
    # loop over all data per cowid and select (x,y,t) data to filter
    for i in range(0,len(cows)):
        # select data
        print("i = " + str(i) + ", cow = " + str(round(cows["cowid"][i])) + ", barn = " +str(data.loc[(data["cowid"] == cows["cowid"][i]),"barn"].mean()))
        cow_t = data.loc[(data["cowid"] == cows["cowid"][i]),"relsec"]
        cow_x = data.loc[(data["cowid"] == cows["cowid"][i]),"X"]
        cow_y = data.loc[(data["cowid"] == cows["cowid"][i]),"y"]
        acc_x = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_x"]
        acc_y = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_y"]
        acc_z = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_z"]
        barn = round(data.loc[(data["cowid"] == cows["cowid"][i]),"barn"].mean())
        
        # filter data
        if len(cow_t) > 10:
            # add edges if t doesn't end at second 86399 of the day
            if cow_t.max() < 86399:
                cow_t = pd.concat([cow_t,pd.Series(86399)], axis=0)
                cow_x = pd.concat([cow_x,pd.Series(np.nan)], axis=0)
                cow_y = pd.concat([cow_y,pd.Series(np.nan)], axis=0)
                acc_x = pd.concat([acc_x,pd.Series(np.nan)], axis=0)
                acc_y = pd.concat([acc_y,pd.Series(np.nan)], axis=0)
                acc_z = pd.concat([acc_z,pd.Series(np.nan)], axis=0)
            
            # add edges if t doesn't start with 0th second of the day
            if cow_t.min() > 0:
                cow_t = pd.concat([pd.Series(0),cow_t], axis=0)
                cow_x = pd.concat([pd.Series(np.nan),cow_x], axis=0)
                cow_y = pd.concat([pd.Series(np.nan),cow_y], axis=0)
                acc_x = pd.concat([pd.Series(np.nan),acc_x], axis=0)
                acc_y = pd.concat([pd.Series(np.nan),acc_y], axis=0)
                acc_z = pd.concat([pd.Series(np.nan),acc_z], axis=0)
            
            cow_t = cow_t.reset_index(drop=1)
            cow_x = cow_x.reset_index(drop=1)
            cow_y = cow_y.reset_index(drop=1)
            acc_x = acc_x.reset_index(drop=1)
            acc_y = acc_y.reset_index(drop=1)
            acc_z = acc_z.reset_index(drop=1)
            
            # filter barn edges if within 0.5m of edge
            edges = barn_edges.loc[(barn_edges["barn"] == barn),:].reset_index(drop=1)
            x1,y1,x2,y2,x3,y3,x4,y4 = edges[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[0]
            test2 =  cow_x.loc[((cow_x > (x1-0.5)) & (cow_x < x1))] = x1  # left edge x
            cow_x.loc[(cow_x > x2) & (cow_x < x2+0.5)] = x2  # right edge x
            cow_y.loc[(cow_y < y1) & (cow_y > y1-0.5)] = y1  # upper edge y
            cow_y.loc[(cow_y > y3)] = -1.2  # upper edge y
            del x1,y1,x2,y2,x3,y3,x4,y4,edges
            
            # filter with acceleration
            df,x,y = filter_interp_withacc(cow_t,cow_x,cow_y,acc_x,acc_y,acc_z,win_med,gap_thres)
            # select columns and add cowid
            df["cowid"] = cows["cowid"][i]
            df["barn"] = barn
            df["date"] = str(startdate+dd)
            df.loc[df["xnew"].isna(),"xnew"] = df.loc[df["xnew"].isna(),"X"]
            df.loc[df["ynew"].isna(),"ynew"] = df.loc[df["ynew"].isna(),"y"]
            
            #--------------------------------------------------------------
            # if origin of system not changed, can be calculated on all
            if pd.to_datetime(startdate+dd) >= pd.to_datetime("2020-09-01"):
                myarea = area_zones.loc[(area_zones["barn"] == barn) | \
                                        (area_zones["barn"].isna() == True) \
                                        ,:].reset_index(drop=1)
                for j in range(0,len(myarea)):
                    x1,y1,x2,y2,x3,y3,x4,y4 = myarea[["x1","y1","x2","y2","x3","y3","x4","y4"]].values[j]
                    df.loc[((df["xnew"] >= x1) & (df["xnew"] <= x2) & \
                           (df["xnew"] >= x3) & (df["xnew"] <= x4) & \
                           (df["ynew"] >= y1) & (df["ynew"] >= y2) & \
                           (df["ynew"] <= y3) & (df["ynew"] <= y4)),"area"] = myarea["area"][j]
                    df.loc[((df["xnew"] >= x1) & (df["xnew"] <= x2) & \
                           (df["xnew"] >= x3) & (df["xnew"] <= x4) & \
                           (df["ynew"] >= y1) & (df["ynew"] >= y2) & \
                           (df["ynew"] <= y3) & (df["ynew"] <= y4)),"zone"] = myarea["zone"][j]
                    del x1,y1,x2,y2,x3,y3,x4,y4
                    
            df = df[["cowid","barn","date","t","gap","X","y","xnew","ynew","acc_x","acc_y","acc_z","dist","area","zone"]]
            #test = df.loc[(df["area"].isna()) & (df["dist"] < 180),:]   # check no area
            #plt.scatter(test["xnew"],test["ynew"],marker = "o",c = "blue",s = 4)
            del x, y
        else:
            df = pd.DataFrame({"cowid":pd.Series(np.ones(86400)*cows["cowid"][i]),
                               "barn":pd.Series(np.ones(86400)*barn),
                               "date":str(startdate+dd),
                               "t" : pd.Series(np.arange(0,86400,1)),
                               "gap" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "X" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "xnew" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "ynew" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "acc_x" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "acc_y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "acc_z" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "dist" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "area" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "zone" : pd.Series(np.arange(0,86400,1)*np.nan)
                               })
            
        # write data to csv per cow
        df.to_csv(path_res + "//barn" + str(round(barn))+ "//data_" + str(startdate+dd).replace("-","") + "_barn_" + str(round(barn)) + "_cow_"+ str(cows["cowid"][i])  + ".txt", index=False)
        del df, cow_t, cow_x, cow_y, acc_x, acc_y, acc_z

    # clear workspace and memory
    del data
#except:
 #   print("data preprocessing for " + str(startdate+dd).replace("-","") + " has failed ")
  #  pass
    
    
# #%% data from sql lite database
# """
# Milk production = TVL_total
# Milk composition / SCC = TVL_total
# Health registers = Agrovision_Treatments & Agrovision_Diagnosis
# Reproduction information = TVL_total
# """


# # set connection to sql database
# connection = sqlite3.connect(path_sql+"//4400003406_research.sqlite")

# # diagnoses and treatments
# treat = pd.read_sql('SELECT * FROM Agrovision_Treatments', connection)
# diag = pd.read_sql('SELECT * FROM Agrovision_Diagnosis', connection)

# # read milking information
# milk = pd.read_sql('SELECT * FROM TVL_Total', connection)
# col = ["Dat","Hok","LevNum","OorNum","AnimalServerId",
#        "LftDgn","KlfDat","LacDgn",
#        "InsDat","DrgZetDat","VrwKlfDat","GewO", "GewA",
#        "bcs","KgMlkO","KgMlkA","KgMlkEtm",
#        "VetStd","EiwStd","LacStd","CelStd",
#        "mpr_dag.Fat_Concentration","mpr_dag.Protein_Concentration",
#        "mpr_dag.Lactose_Concentration",
#        "mpr_dag.SCC",
#        "mpr_week.Fat_Concentration","mpr_week.Protein_Concentration",
#        "mpr_week.Lactose_Concentration",
#        "mpr_week.SCC"]
# milk = milk[col]
# milk = milk.sort_values(by=["OorNum","LftDgn"]).reset_index(drop=1)
 
    
# # write to txt file
# today = str(date.today())
# today = today.replace("-","")
# milk.to_csv(path_res+"milk_"+today+"_bec3.txt")
# treat.to_csv(path_res+"treatment_"+today+"_bec3.txt")
# diag.to_csv(path_res+"diagnosis_"+today+"_bec3.txt")
    
