# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 11:42:04 2022

@author: adria036
"""
#%% import modules

import os
from datetime import date, timedelta
import pandas as pd
import numpy as np
import copy

import matplotlib.pyplot as plt
import sqlite3

# set path to data folder
path = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_total"
    )

connection = sqlite3.connect(path+"//4400003406_research.sqlite")
df = pd.read_sql('SELECT * FROM Agrovision_Treatments', connection)
df.head()


