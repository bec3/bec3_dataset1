#!/bin/bash
#-----------------------------Mail address-----------------------------
#SBATCH --mail-user=ines.adriaens@wur.nl
#SBATCH --mail-type=ALL
#-----------------------------Output files-----------------------------
#SBATCH --job-name=NLAS_dataprep
#SBATCH --output=output_%j.txt
#SBATCH --error=error_output_%j.txt
#-----------------------------Other information------------------------
#SBATCH --comment=NaN
#-----------------------------Required resources-----------------------
#SBATCH --time=6:00:00
#SBATCH --mem=16000
#-----------------------------Environment, Operations and Job steps-----------
#your modules
#module unload python/3.7.4
module load python/3.9.4
source /home/WUR/adria036/envs/nlas/bin/activate
cd NLAS/bec3_dataset1/datapreparation
#your job
time python3 data_preparation_LF.py