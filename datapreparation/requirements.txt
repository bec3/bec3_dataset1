pyarrow=8.0.0
python-snappy=0.6.0
pandas=1.4.0
datetime=4.3
copy
numpy=1.22.1
sqlite=3.37.0
scipy=1.8.0
fastparquet=0.5.0
openpyxl=3.0.9
