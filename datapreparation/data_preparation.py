# -*- coding: utf-8 -*-
"""
Created on Thu Jul 14 11:47:33 2022

@author: adria036

-------------------------------------------------------------------------------
Script for data preparation


- UWB data, 1/s, imputed, filtered
- Milk production data
- Treatments, reproduction
- MPR data


"""

import os
os.chdir(r"C:\Users\adria036\OneDrive - Wageningen University & Research\iAdriaens_doc\Projects\iAdriaens\BEC3\scripts\bec3_dataset1\datapreparation") 


#%% import modules

import os
from datetime import date, timedelta
import pandas as pd
import numpy as np
import copy
#import matplotlib.pyplot as plt
from filter_data import filter_barn_edges, filter_interp_withacc
from read_data_sewio import read_all_data, read_ids, combine_data_id
import barn_areas
import sqlite3


#%% set filepaths and dates

# set path to data X,y
path_os = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_sewio"
    )

# set path to excel file with cowids
path = path_os

# set path to sql data folder
path_sql = os.path.join(
    "W:","ASG","WLR_Dataopslag","DairyCampus","3406_Nlas","raw","dc_total"
    )

# set path to results folder
path_res = os.path.join("C:","/Users","adria036","OneDrive - Wageningen University & Research",
                        "iAdriaens_doc","Projects","iAdriaens","BEC3","data/"
                        )

# set file name for cowid information
fn = "\Copy_of_Sewio_tags.xlsx"

# set start and end dates
startdate = date(2022,3,17)
enddate = date(2022,3,31)

# set parameters for the imputation and interpolation
win_med = 31
gap_thres = 180


#%% write loop for data loading, editing and saving
t_interval = enddate - startdate
for dd in range(t_interval.days+1):
    dd_0 = timedelta(dd-1)
    dd = timedelta(dd)
    print(startdate+dd)
    
    # --------------------------read & select data-----------------------------
    try:
        data = read_all_data(startdate+dd_0,startdate+dd,path_os)
    
        # read id/tag data
        tags = read_ids(path, fn)

        # combine it with data
        data = combine_data_id(data,tags)
        data["date"] = data["at"].dt.date
        data = data.loc[data["date"] == startdate+dd,:]
        # add selection step to correct for time > only day "startdate + dd" retained
        data = data.loc[data["date"] == startdate+dd,:]
            # remaining tags without cowID = during the changing
            # remove those values
        data = data.loc[data["cowid"] != 0,:]
        data = data.sort_values(by=["cowid","at"]).reset_index(drop=1)

        
        data = data[["cowid","alias","barn","date","at","feed_reference","title","X","y","acc_x","acc_y","acc_z"]]    
        # unique 'seconds' in the dataset 
        #data["day"] = data["at"].dt.day - min(data["at"].dt.day)
        data["hour"] = data["at"].dt.hour
        data["minute"] = data["at"].dt.minute
        data["second"] = data["at"].dt.second
        data["relsec"] = data["hour"]*3600 \
                         + data["minute"]*60 \
                         + data["second"]
        nsec = data["relsec"]
        cowids = data["cowid"]
        df = pd.concat([cowids,nsec],axis = 1).drop_duplicates()  # gives the indices of unique first 
        del df["relsec"], df["cowid"]
        
        # innerjoin for selection step - unique seconds in the dataset
        data = pd.concat([data,df],axis = 1, join = 'inner')
        data = data.sort_values(by = ["cowid","at"]).reset_index(drop=1)
        
        # unfiltered data - one per second or less, all cows included  
        # calculate gaps based on numeric time relative to start of dataset
        data["gap"] = data["relsec"].diff()
        data.loc[data["gap"] < 0,"gap"] = np.nan # set to nan when different cowid

        #--------------------------edit barn edges---------------------------------
        # set data outside edges to edges x < 0 and < 10.6 or x < 21 and x > 32
        data.loc[(data["X"] < 15),"X"] = filter_barn_edges( \
                 copy.copy(data.loc[(data["X"] < 15),"X"]),0,10.6) #barn 72
        data.loc[(data["X"] >= 15),"X"] = filter_barn_edges( \
                  copy.copy(data.loc[(data["X"] >= 15),"X"]),21,32) # barn 70

        # set data outside edges to edges y < -18 or y > -2.5
        data["y"] = filter_barn_edges(copy.copy(data["y"]),-18,-2.5)

        #data["X"].hist()
        #data["y"].hist()
        
        # delete rows without cowid
        data = data.loc[data["cowid"]>0,:]

        #-----------------------filter & interpolate per cow-----------------------
        # filter the (X,y) time series with a median filter and interpolate
        
        # cows in the barn
        cows = data["cowid"].drop_duplicates().sort_values().reset_index(drop=1)
        cows = pd.DataFrame(cows)
        
        # loop over all data per cowid and select (x,y,t) data to filter
        result = pd.DataFrame({"cowid": [],
                               "date": [],
                               "t" : [],
                               "gap" :  [],
                               "X" :  [],
                               "y" :  [],
                               "xnew" : [],
                               "ynew" : [],
                               "area" : [],
                               "acc_x" : [],
                               "acc_y" : [],
                               "acc_z" : []})
        for i in range(0,len(cows)):
            # select data
            print(i)
            cow_t = data.loc[(data["cowid"] == cows["cowid"][i]),"relsec"]
            cow_x = data.loc[(data["cowid"] == cows["cowid"][i]),"X"]
            cow_y = data.loc[(data["cowid"] == cows["cowid"][i]),"y"]
            acc_x = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_x"]
            acc_y = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_y"]
            acc_z = data.loc[(data["cowid"] == cows["cowid"][i]),"acc_z"]
            
            
            # filter data
            if len(cow_t) > 10:
                # add edges if t doesn't end at second 86399 of the day
                if cow_t.max() < 86399:
                    cow_t = pd.concat([cow_t,pd.Series(86399)], axis=0)
                    cow_x = pd.concat([cow_x,pd.Series(np.nan)], axis=0)
                    cow_y = pd.concat([cow_y,pd.Series(np.nan)], axis=0)
                    acc_x = pd.concat([acc_x,pd.Series(np.nan)], axis=0)
                    acc_y = pd.concat([acc_y,pd.Series(np.nan)], axis=0)
                    acc_z = pd.concat([acc_z,pd.Series(np.nan)], axis=0)
                
                # add edges if t doesn't start with 0th second of the day
                if cow_t.iloc[0]/86400 - float(cow_t.iloc[0]//86400) > 0.00001:
                    cow_t = pd.concat([pd.Series(cow_t.iloc[0]//86400*86400),cow_t], axis=0)
                    cow_x = pd.concat([pd.Series(np.nan),cow_x], axis=0)
                    cow_y = pd.concat([pd.Series(np.nan),cow_y], axis=0)
                    acc_x = pd.concat([pd.Series(np.nan),acc_x], axis=0)
                    acc_y = pd.concat([pd.Series(np.nan),acc_y], axis=0)
                    acc_z = pd.concat([pd.Series(np.nan),acc_z], axis=0)
                
                # filter
                # df,x,y = filter_interp(cow_t,cow_x,cow_y,win_med,gap_thres)
                df,x,y = filter_interp_withacc(cow_t,cow_x,cow_y,acc_x,acc_y,acc_z,win_med,gap_thres)
                df = df.reset_index(drop=1)
                
                # asign barn areas
                data_area, parea = barn_areas.assign_behaviour(x,y)
                data_area = data_area[~data_area.index.duplicated(keep='first')]
                
                # merge with df
                df["area"] = data_area["no"]
                
                # select columns and add cowid
                df["cowid"] = cows["cowid"][i]
                df["date"] = str(startdate+dd)
                df = df[["cowid","date","t","gap","X","y","acc_x","acc_y","acc_z","xnew","ynew","area"]]
            else:
                df = pd.DataFrame({"cowid":cows["cowid"][i],
                               "date":str(startdate+dd),
                               "t" : pd.Series(np.arange(0,86400,1)),
                               "gap" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "X" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "acc_x" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "acc_y" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "acc_z" :  pd.Series(np.arange(0,86400,1)*np.nan),
                               "xnew" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "ynew" : pd.Series(np.arange(0,86400,1)*np.nan),
                               "area" : pd.Series(np.arange(0,86400,1)*np.nan)})
                
            # concat dataframes all cows together 
            result = pd.concat([result,df])

        # save results
        datestr = str(startdate+dd)
        datestr = datestr.replace("-","")    
        fns =  datestr + '_bec3.txt'
        result.to_csv(path_res + fns, index = False)
        
        # clear workspace and memory
        del data, df, datestr, fns, result, data_area, x, y, cow_t, cow_x, cow_y, parea, cows, nsec
    except:
        pass
    
    
#%% data from sql lite database
"""
Milk production = TVL_total
Milk composition / SCC = TVL_total
Health registers = Agrovision_Treatments & Agrovision_Diagnosis
Reproduction information = TVL_total
"""


# set connection to sql database
connection = sqlite3.connect(path_sql+"//4400003406_research.sqlite")

# diagnoses and treatments
treat = pd.read_sql('SELECT * FROM Agrovision_Treatments', connection)
diag = pd.read_sql('SELECT * FROM Agrovision_Diagnosis', connection)

# read milking information
milk = pd.read_sql('SELECT * FROM TVL_Total', connection)
col = ["Dat","Hok","LevNum","OorNum","AnimalServerId",
       "LftDgn","KlfDat","LacDgn",
       "InsDat","DrgZetDat","VrwKlfDat","GewO", "GewA",
       "bcs","KgMlkO","KgMlkA","KgMlkEtm",
       "VetStd","EiwStd","LacStd","CelStd",
       "mpr_dag.Fat_Concentration","mpr_dag.Protein_Concentration",
       "mpr_dag.Lactose_Concentration",
       "mpr_dag.SCC",
       "mpr_week.Fat_Concentration","mpr_week.Protein_Concentration",
       "mpr_week.Lactose_Concentration",
       "mpr_week.SCC"]
milk = milk[col]
milk = milk.sort_values(by=["OorNum","LftDgn"]).reset_index(drop=1)
 
    
# write to txt file
today = str(date.today())
today = today.replace("-","")
milk.to_csv(path_res+"milk_"+today+"_bec3.txt")
treat.to_csv(path_res+"treatment_"+today+"_bec3.txt")
diag.to_csv(path_res+"diagnosis_"+today+"_bec3.txt")
    
    
    
    
    
    
    
    
    
    