# BEC3

__Data preparation for analysis of Roxann__

### Context

__created__: 14/7/2022_
__created by__: Ines Adriaens

### Dataset to be constructed

- spatial data gefiltered + imputed 1/s
- Milk production
- Milk composition / SCC
- Health registers
- Reproduction information


Initially select data from April 2022 onwards, update expected after summer