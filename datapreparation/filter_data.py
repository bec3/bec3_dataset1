# -*- coding: utf-8 -*-
"""
Created on Mon May 23 13:22:02 2022

@author: adria036
"""




#import os
import pandas as pd
import numpy as np
#import copy
#import matplotlib.pyplot as plt
from scipy.signal import medfilt, medfilt2d
import scipy

def non_uniform_savgol(x, y, window, polynom):
    """
    Applies a Savitzky-Golay filter to y with non-uniform spacing
    as defined in x

    This is based on https://dsp.stackexchange.com/questions/1676/savitzky-golay-smoothing-filter-for-not-equally-spaced-data
    The borders are interpolated like scipy.signal.savgol_filter would do

    Parameters
    ----------
    x : array_like
        List of floats representing the x values of the data
    y : array_like
        List of floats representing the y values. Must have same length
        as x
    window : int (odd)
        Window length of datapoints. Must be odd and smaller than x
    polynom : int
        The order of polynom used. Must be smaller than the window size

    Returns
    -------
    np.array of float
        The smoothed y values
    """
    if len(x) != len(y):
        raise ValueError('"x" and "y" must be of the same size')

    if len(x) < window:
        raise ValueError('The data size must be larger than the window size')

    if type(window) is not int:
        raise TypeError('"window" must be an integer')

    if window % 2 == 0:
        raise ValueError('The "window" must be an odd integer')

    if type(polynom) is not int:
        raise TypeError('"polynom" must be an integer')

    if polynom >= window:
        raise ValueError('"polynom" must be less than "window"')

    half_window = window // 2
    polynom += 1

    # Initialize variables
    A = np.empty((window, polynom))     # Matrix
    tA = np.empty((polynom, window))    # Transposed matrix
    t = np.empty(window)                # Local x variables
    y_smoothed = np.full(len(y), np.nan)

    # Start smoothing
    N = 0
    for i in range(half_window, len(x) - half_window, 1):
        # print(i)
        N += 1
        
        # Center a window of x values on x[i]
        for j in range(0, window, 1):
            #print(j)
            t[j] = x[i + j - half_window] - x[i]
            
            #NEW: t2 with stepsize 1
            t2 = list(range(x[i - half_window] - x[i], x[i + (window-1) - half_window] - x[i]+1))
            t2 = np.asarray(t2,dtype = float)
        
        #NEW: Initialize variables based on variable window
        A2 = np.empty((len(t2), polynom))     # Matrix
        # tA2 = np.empty((polynom, len(t2)))    # Transposed matrix
            
        # Create the initial matrix A and its transposed form tA
        for j in range(0, window, 1):
            r = 1.0
            for k in range(0, polynom, 1):
                A[j, k] = r
                tA[k, j] = r
                r *= t[j]
                
        for j in range(0,len(t2),1):
            # print(j,t2[j])
            r = 1.0
            for k in range(0, polynom, 1):
                A2[j, k] = r
                # tA2[k, j] = r
                r *= t2[j]

        # Multiply the two matrices = (A^T*A)
        tAA = np.matmul(tA, A)

        # Invert the product of the matrices =(A^T*A)-1
        tAA = np.linalg.inv(tAA)

        # Calculate the pseudoinverse of the design matrix = (A^T*A)-1*A^T 
        coeffs = np.matmul(tAA, tA)
        
        #NEW: Calculate c by matrix multiplying it with the original known y values
        #NEW: in the window of interest
        c = np.matmul(coeffs,y[i-half_window:i+window-1-half_window+1])
        
        #NEW: c[0] = is the smoothed value for i
        #NEW: we can calculate the interpolated values using y = A2*c
        y_interp = np.matmul(A2,c)
        
        #NEW: rearrange to return - select the values needed for the smoothing
        #NEW: based on the frames that we have sampled
        if N == 1:
            new_data = pd.DataFrame(t2,columns = ["tstep"])
            new_data["frameno"] = new_data["tstep"] + x[i]
            new_data["y_smooth"+str(N)] = y_interp
            new_data = new_data.drop("tstep",axis = 1)
        else:
            sm_data = pd.DataFrame(t2,columns = ["tstep"])
            sm_data["frameno"] = sm_data["tstep"] + x[i]
            sm_data["y_smooth"+str(N)] = y_interp
            sm_data = sm_data.drop("tstep", axis = 1)
            new_data = new_data.merge(sm_data,how='outer',on='frameno')
                
        # Calculate c0 which is also the y value for y[i] based on available only
        y_smoothed[i] = 0
        for j in range(0, window, 1):
            y_smoothed[i] += coeffs[0, j] * y[i + j - half_window]
            
        # If at the end or beginning, store all coefficients for the polynom
        if i == half_window:
            first_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    first_coeffs[k] += coeffs[k, j] * y[j]
        elif i == len(x) - half_window - 1:
            last_coeffs = np.zeros(polynom)
            for j in range(0, window, 1):
                for k in range(polynom):
                    last_coeffs[k] += coeffs[k, j] * y[len(y) - window + j]

    # Interpolate the result at the left border
    for i in range(0, half_window, 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += first_coeffs[j] * x_i
            x_i *= x[i] - x[half_window]

    # Interpolate the result at the right border
    for i in range(len(x) - half_window, len(x), 1):
        y_smoothed[i] = 0
        x_i = 1
        for j in range(0, polynom, 1):
            y_smoothed[i] += last_coeffs[j] * x_i
            x_i *= x[i] - x[half_window - 1]
            
    #NEW: compute interpolated mean to sample when not available
    new_data["mean"] = new_data.iloc[:,1:].mean(axis=1)
    y_all = new_data[["frameno","mean"]]

    return y_smoothed, y_all
    
def filter_barn_edges(ts,edge1,edge2):
    """
    Filters the data according to barn edges given by edge1 and edge2
    Method: puts data outside edges to nan
        if ts < edge1 : ts = nan
        if ts > edge2 : ts = nan
    NOTE: in a later stage, we can decide to put ts  = closest edge

    Parameters
    ----------
    ts : TYPE = Pandas series (time series)
        DESCRIPTION.
    edge1 : TYPE
        DESCRIPTION.
    edge2 : TYPE
        DESCRIPTION.

    Returns
    -------
    ts_filt : TYPE = Pandas series
        Original series filtered for measurements outside the edges

    """
    #--------------------------------------------------------------------------
    # for development only (delete)
        #ts = copy.copy(data["X"])
        #edge1 = 0
        #edge2 = 32
    #--------------------------------------------------------------------------
    
    # correction1: if more than 30 cm out = nan
    ts.loc[ts <= edge1+0.3] = np.nan
    ts.loc[ts >= edge2+0.3] = np.nan    
    
    # correction2: if less than 30 outside = edge
    ts.loc[(ts > edge1-0.3) & (ts < edge1)] = edge1
    ts.loc[(ts < edge2+0.3) & (ts > edge2)] = edge2
    
    
    return ts


def filter_interp(t,x,y,win_med,gap_thres):
    """
    Filters the x,y data by linear interpolation across median filtered data
    Returns 'completed' time series between edges
    If gap larger than threshold: no imputation
    If gap larger than win_med/2: no median filter
    

    Parameters
    ----------
    t : TYPE = pandas Series
        time series of time values expressed in relative seconds
    x : TYPE = pandas Series
        time series of x values that needs imputation
    y : TYPE  = pandas Series
        time series of y values that needs imputation
    win_med : TYPE = Int/float
        Window for the median filter (kernel size)
    gap_thres : TYPE = Int/float
        gap threshold for when data imputation is not reliable anymore

    Returns
    -------
    df3 : TYPE = Pandas DataFrame
        DataFrame with imputed, filtered and completed data. From these,
        barn area and time budget allocations can be calculated.
    """

    # construct dataframe
    df = pd.DataFrame({'X' : x,
                       'y' : y,
                       't' : t})
    # calculate gaps from t
    df["gap"] = t.diff()
    df = df.reset_index(drop=1)
    
    # find gapsize < window/2 seconds -- only apply median filter to these
    gapind = df["gap"].loc[df["gap"]>round(win_med/2)].index #startindex
    gapind = pd.DataFrame(np.append(gapind,0))
    gapind = gapind.sort_values(by=0).reset_index(drop=1)
    gapind2 = pd.DataFrame(np.append(df["gap"].loc[df["gap"]>round(win_med/2)].index,len(df)-1)) #endindex
    gapind2 = gapind2.sort_values(by=0).reset_index(drop=1)
    
    # prepare calculations
    df["xfilt"] = np.nan
    df["yfilt"] = np.nan
    
    # calculate median depending on gapsize
    for i in range(0,len(gapind)):
        # print(gapind.iloc[i,0],gapind2.iloc[i,0])
        if gapind2.iloc[i,0]-gapind.iloc[i,0] > win_med:
            # median filter
            a = medfilt(df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=win_med)
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = a
            a = medfilt(df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]],kernel_size=win_med)
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = a
        else:
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = \
                df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = \
                df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
    
    # linear (x,y) interpolation based on xfilt and yfilt
    tmin = df["t"].min()
    tmax = df["t"].max()
    tnew = np.linspace(tmin,tmax,tmax-tmin).round()
    
    # do the interpolation
    f = scipy.interpolate.interp1d(df["t"],[df["xfilt"],df["yfilt"]],kind="linear")
    xnew,ynew = f(tnew)
    
    # to dataframe
    df2 = pd.DataFrame({'t' : tnew,
                        'xnew' : xnew,
                        'ynew' : ynew})
    
    # merge with df based on key = t and tnew
    df3 = pd.merge(df2,df,
                   how = 'outer',
                   on = 't').sort_values(by="t").reset_index(drop=1)
    
    # find gapsize > gap_thres seconds -- to delete data
    gapind = df3["gap"].loc[df3["gap"]>gap_thres].index
    gapind2 = df3["gap"].loc[df3["gap"]>gap_thres]
    gaps = pd.DataFrame({'indx' : gapind,
                          'gapsize' : gapind2}).reset_index(drop=1)
        
    # delete xnew and ynew if gap > gap_thresh
    #TODO: this can probably be more efficient with merging/indexing
    for i in range(0,len(gaps)):
        # print(i)
        df3.loc[int(gaps["indx"][i]-gaps["gapsize"][i]+1): \
                int(gaps.indx[i])-1,["xnew","ynew"]] = np.nan    
    
    return df3, df3["xnew"], df3["ynew"]



def filter_interp_withacc(t,x,y,acc_x,acc_y,acc_z,win_med,gap_thres):
    """
    Filters the x,y data by linear interpolation across median filtered data
    Returns 'completed' time series between edges
    If gap larger than threshold: no imputation
    If gap larger than win_med/2: no median filter
    

    Parameters
    ----------
    t : TYPE = pandas Series
        time series of time values expressed in relative seconds
    x : TYPE = pandas Series
        time series of x values that needs imputation
    y : TYPE  = pandas Series
        time series of y values that needs imputation
    win_med : TYPE = Int/float
        Window for the median filter (kernel size)
    gap_thres : TYPE = Int/float
        gap threshold for when data imputation is not reliable anymore

    Returns
    -------
    df3 : TYPE = Pandas DataFrame
        DataFrame with imputed, filtered and completed data. From these,
        barn area and time budget allocations can be calculated.
    """

    # construct dataframe
    df = pd.DataFrame({'X' : x,
                       'y' : y,
                       't' : t,
                       'acc_x' : acc_x,
                       'acc_y' : acc_y,
                       'acc_z' : acc_z})
    # calculate gaps from t
    df["gap"] = t.diff()
    df = df.reset_index(drop=1)
    
    # find gapsize < window/2 seconds -- only apply median filter to these
    gapind = df["gap"].loc[df["gap"]>round(win_med/2)].index #startindex
    gapind = pd.DataFrame(np.append(gapind,0))
    gapind = gapind.sort_values(by=0).reset_index(drop=1)
    gapind2 = pd.DataFrame(np.append(df["gap"].loc[df["gap"]>round(win_med/2)].index,len(df)-1)) #endindex
    gapind2 = gapind2.sort_values(by=0).reset_index(drop=1)
    
    # prepare calculations
    df["xfilt"] = np.nan
    df["yfilt"] = np.nan
    
    # calculate median depending on gapsize
    for i in range(0,len(gapind)):
        # print(gapind.iloc[i,0],gapind2.iloc[i,0])
        if gapind2.iloc[i,0]-gapind.iloc[i,0] > win_med:
            # median filter
            a = medfilt2d(df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],["X","y"]],kernel_size = [win_med,1])
            df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],["xfilt","yfilt"]] = a

        # else:
        #     df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"xfilt"] = \
        #         df["X"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
        #     df.loc[gapind.iloc[i,0]:gapind2.iloc[i,0],"yfilt"] = \
        #         df["y"].loc[gapind.iloc[i,0]:gapind2.iloc[i,0]]
    
    df.loc[df["yfilt"].isna(),"yfilt"] = df.loc[df["yfilt"].isna(),"y"].values
    df.loc[df["xfilt"].isna(),"xfilt"] = df.loc[df["xfilt"].isna(),"X"].values
    
    
    # linear (x,y) interpolation based on xfilt and yfilt
    tmin = df["t"].min()
    tmax = df["t"].max()
    tnew = np.linspace(tmin,tmax,tmax-tmin+1)
    
    # do the interpolation
    f = scipy.interpolate.interp1d(df["t"],[df["xfilt"],df["yfilt"]],kind="linear")
    xnew,ynew = f(tnew)
    
    # to dataframe
    df2 = pd.DataFrame({'t' : tnew,
                        'xnew' : xnew,
                        'ynew' : ynew})
    
    # merge with df based on key = t and tnew
    df3 = pd.merge(df2,df,
                   how = 'outer',
                   on = 't').sort_values(by="t").reset_index(drop=1)
    df3.loc[df3["xnew"].isna(),["xnew","ynew"]] = df3.loc[df3["xnew"].isna(),["xfilt","yfilt"]].values
        
    # find all values that are larger than "gap_threshold"
    gaps = df3.loc[(df3["gap"] > gap_thres),:].copy()
    
    # calculate distance travelled during gap > gap_threshold
    if len(gaps)>0:
        distances = np.sqrt((gaps["xnew"][:-1].values-gaps["xnew"][1:].values)**2 + \
                            (gaps["ynew"][:-1].values-gaps["ynew"][1:].values)**2)
        gaps.loc[:,"dist"] = np.nan
        gaps["dist"].values[1:] = distances
        df3 = pd.concat([df3,gaps[["dist"]]], axis = 1)
        # backfill to check which data needs to be deleted (distance > 1 and gap > gap_threshold)
        df3["gap"] = df3["gap"].fillna(method = "bfill")   
        df3["dist"] = df3["dist"].fillna(method = "bfill") 
        # delete xnew and ynew if gap > gap_thresh and distance > 1
        df3.loc[(df3["gap"]>gap_thres) &(df3["dist"]>1) ,"xnew"] = np.nan
        df3.loc[(df3["gap"]>gap_thres) &(df3["dist"]>1) ,"ynew"] = np.nan
   
    # calculate minimal distance travelled and correct accordingly
    p1 = (df3["xnew"].values[1:] - df3["xnew"].values[:-1])**2
    p2 = (df3["ynew"].values[1:] - df3["ynew"].values[:-1])**2
    df3["dist"] = pd.DataFrame(np.sqrt(p1+p2),columns = ["dist"], index = df3.index[1:])
    df3.loc[df3["dist"] > 2, "dist"] = 2
    df3.loc[df3["dist"] < 0.2, "dist"] = 0   # minimum distance travelled
    
    return df3, df3["xnew"], df3["ynew"]