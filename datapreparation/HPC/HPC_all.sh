#!/bin/bash
## ----------------------settings----------------------
#SBATCH --comment=
#SBATCH --time=200
#SBATCH --mem=16000
#SBATCH --ntasks=1
#SBATCH --output=output_%j.txt
#SBATCH --error=error_output_%j.txt
#SBATCH --job-name=run_all_scripts
#SBATCH --partition=main
#SBATCH --mail-type=ALL
#SBATCH --mail-user=ines.adriaens@wur.nl

## -----------------------code---------------------------
time for i in {1..16}; do echo $i; dos2unix data_preparation_LF$i.sh; sbatch data_preparation_LF$i.sh; done